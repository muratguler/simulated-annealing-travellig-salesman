import matplotlib.pyplot as plt

def GezginSaticiGorseli(yollar, noktalar, yenileme_sirasi=1):

    """
    yol: Düğümlerin ziyaret edildiği farklı sıralara sahip listelerin listesi
    noktalar: farklı düğümler için koordinatlar
    yenileme_sirasi: yol listesindeki yolların sayısı

    """

    # Birinci Gezgin Satıcı yolunu aç ve onu sipariş edilenler listesine dönüştür.
    # koordinatlar

    x = []; y = []
    for i in yollar[0]:
        x.append(noktalar[i][0])
        y.append(noktalar[i][1])

    plt.plot(x, y, 'co')

    # Ok başları için bir ölçek ayarlar (bunun için makul bir temerrüt olmalı, WTF?)
    olcek = float(max(x))/float(100)

    # Varsa, daha eski yolları çizer
    if yenileme_sirasi > 1:

        for i in range(1, yenileme_sirasi):

            # Eski yolları bir koordinat listesine dönüştürür
            xi = []; yi = [];
            for j in yollar[i]:
                xi.append(noktalar[j][0])
                yi.append(noktalar[j][1])

            plt.arrow(xi[-1], yi[-1], (xi[0] - xi[-1]), (yi[0] - yi[-1]),
                    head_width = olcek, color = 'r',
                    length_includes_head = True, ls = 'dashed',
                    width = 0.001/float(yenileme_sirasi))
            for i in range(0, len(x) - 1):
                plt.arrow(xi[i], yi[i], (xi[i+1] - xi[i]), (yi[i+1] - yi[i]),
                        head_width = olcek, color = 'r', length_includes_head = True,
                        ls = 'dashed', width = 0.001/float(yenileme_sirasi))

    # Gezgin satıcı problemi için birinci yolu çizer
    plt.arrow(x[-1], y[-1], (x[0] - x[-1]), (y[0] - y[-1]), head_width = olcek,
            color ='g', length_includes_head=True)
    for i in range(0,len(x)-1):
        plt.arrow(x[i], y[i], (x[i+1] - x[i]), (y[i+1] - y[i]), head_width = olcek,
                color = 'g', length_includes_head = True)

    #Set axis too slitghtly larger than the set of x and y
    plt.xlim(min(x)*1.1, max(x)*1.1)
    plt.ylim(min(y)*1.1, max(y)*1.1)
    plt.show()