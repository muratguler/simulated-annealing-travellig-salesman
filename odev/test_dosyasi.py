from tavlama import Isil_islem_fonksiyonu
import matplotlib.pyplot as plt
import random

koordinatlar = []
with open("koordinatlar.txt", "r") as f:
    for line in f.readlines():
        line = [float(x.replace("\n", "")) for x in line.split(" ")]
        koordinatlar.append(line)

# ilk çalıştırılacak olan fonksiyon main değil ise çalıştırmaz.  ana
# fonksiyonumuzun adı main olduğu için bizde de bu çalışması lazım.
if __name__ == "__main__":
    # koordinatlar = [[random.uniform(-1000, 1000), random.uniform(-1000,
    # 1000)] for i in range(100)]
    sa = Isil_islem_fonksiyonu(koordinatlar,ilksicaklik=10,alpha=1,durdurma_sicakligi=1, durdurma_iterasyonu=5000)
    sa.tavlama()
    sa.rotalari_gorsellestirme()
    sa.cizim_ogrenmesi()
