import math
import random
import gorsellestirme
import matplotlib.pyplot as plt


class Isil_islem_fonksiyonu(object):
    def __init__(self, koordinatlar, ilksicaklik=-1, alpha=-1, durdurma_sicakligi=-1, durdurma_iterasyonu=-1):
        self.koordinatlar = koordinatlar
        self.koordinatuzunlugu = len(koordinatlar)
        self.ilksicaklik = math.sqrt(self.koordinatuzunlugu) if ilksicaklik == -1 else ilksicaklik
        self.kaydedilen_ilksicaklik = self.ilksicaklik  # ısıl işlem kullanılırsa ilk sicakligi kayıt ediyoruz.
        self.alpha = 0.995 if alpha == -1 else alpha
        self.durdurma_isi_derecesi = 1e-8 if durdurma_sicakligi == -1 else durdurma_sicakligi
        self.durdurma_iterasyonu = 100000 if durdurma_iterasyonu == -1 else durdurma_iterasyonu
        self.yenileme = 1

        self.dugumler = [i for i in range(self.koordinatuzunlugu)]

        self.eniyi_cozum = None
        self.eniyi_uygunluk = float("Inf")
        self.uygunluk_listesi = []

    def ilk_cozum(self):
        """
        Aç gözlü algoritma ilk çözümü alır ( en yakın komşudan ) 
        """
        suanki_dugum = random.choice(self.dugumler)  # rasgele düğümden başlar
        cozum = [suanki_dugum]

        bos_dugumler = set(self.dugumler)
        bos_dugumler.remove(suanki_dugum)
        while bos_dugumler:
            sonraki_dugum = min(bos_dugumler, key=lambda x: self.mesafebul(suanki_dugum, x))  # en yakın komşu düğüm
            bos_dugumler.remove(sonraki_dugum)
            cozum.append(sonraki_dugum)
            suanki_dugum = sonraki_dugum

        suanki_uygunluk = self.uygunluk_fonksiyonu(cozum)
        if suanki_uygunluk < self.eniyi_uygunluk:  
            self.eniyi_uygunluk = suanki_uygunluk
            self.eniyi_cozum = cozum
        self.uygunluk_listesi.append(suanki_uygunluk)
        return cozum, suanki_uygunluk

    def mesafebul(self, dugum_0, dugum_1):
        """
        iki düğüm arasındaki öklid mesafesi
        """
        koordinat_0, koordinat_1 = self.koordinatlar[dugum_0], self.koordinatlar[dugum_1]
        return math.sqrt((koordinat_0[0] - koordinat_1[0]) ** 2 + (koordinat_0[1] - koordinat_1[1]) ** 2)

    def uygunluk_fonksiyonu(self, cozum):
        """
        güncel çözümün toplam mesafesi
        """
        suanki_uygunluk = 0
        for i in range(self.koordinatuzunlugu):
            suanki_uygunluk += self.mesafebul(cozum[i % self.koordinatuzunlugu], cozum[(i + 1) % self.koordinatuzunlugu])
        return suanki_uygunluk

    def kabuletme_olasiligi(self, aday_uygunluk):
        """
         Adayın mevcut olandan daha kötü olup olmadığını kabul etme olasılığı.
         Mevcut sıcaklığa ve aday ile akım arasındaki farka bağlıdır.
        """
        return math.exp(-abs(aday_uygunluk - self.suanki_uygunluk) / self.ilksicaklik)

    def kabuletme(self, aday):
        """
        Aday akımdan daha iyi ise, olasılık 1 ile kabul edin.
        Olasılık daha kötü ise kabuletme_olasiligi(..) ile kabul et
        """
        aday_uygunluk = self.uygunluk_fonksiyonu(aday)
        if aday_uygunluk < self.suanki_uygunluk:
            self.suanki_uygunluk, self.suanki_cozum = aday_uygunluk, aday
            if aday_uygunluk < self.eniyi_uygunluk:
                self.eniyi_uygunluk, self.eniyi_cozum = aday_uygunluk, aday
        else:
            if random.random() < self.kabuletme_olasiligi(aday_uygunluk):
                self.suanki_uygunluk, self.suanki_cozum = aday_uygunluk, aday

    def tavlama(self):
        """
        Isıl işlem algoritmasını çalıştırıyoruz.
        """
        # Aç gözlü çözüm ile başlatılıyor.
        self.suanki_cozum, self.suanki_uygunluk = self.ilk_cozum()

        print("Tavlama başlıyor.")
        while self.ilksicaklik >= self.durdurma_isi_derecesi and self.yenileme < self.durdurma_iterasyonu:
            aday = list(self.suanki_cozum)
            l = random.randint(2, self.koordinatuzunlugu - 1)
            i = random.randint(0, self.koordinatuzunlugu - l)
            aday[i : (i + l)] = reversed(aday[i : (i + l)])
            self.kabuletme(aday)
            self.ilksicaklik *= self.alpha
            self.yenileme += 1

            self.uygunluk_listesi.append(self.suanki_uygunluk)

        print("En iyi uygunluk elde edildi: ", self.eniyi_uygunluk)
        ilerleme = 100 * (self.uygunluk_listesi[0] - self.eniyi_uygunluk) / (self.uygunluk_listesi[0])
        print(f"Açgözlü heuristic üzerinden ilerleme: {ilerleme : .2f}%")

    def yigin_tavlama(self, dondurmesayisi=10):
        """
        Rasgele ilk çözümlerle simüle edilmiş tavlama algoritması 'dondurmesayisi' kadar yürütülür.
        """
        for i in range(1, dondurmesayisi + 1):
            print(f"Yenileme {i}/{dondurmesayisi} -------------------------------")
            self.ilksicaklik = self.kaydedilen_ilksicaklik
            self.yenileme = 1
            self.suanki_cozum, self.suanki_uygunluk = self.ilk_cozum()
            self.tavlama()

    def rotalari_gorsellestirme(self):
        """
        matplotlib ile gezgin satıcı yolu görselleştiriliyor.
        """
        gorsellestirme.GezginSaticiGorseli([self.eniyi_cozum], self.koordinatlar)

    def cizim_ogrenmesi(self):
        """
        Yenilemelerin uygunluk grafiği çiziliyor.
        """
        plt.plot([i for i in range(len(self.uygunluk_listesi))], self.uygunluk_listesi)
        plt.ylabel("Uygunluk")
        plt.xlabel("Yenileme Sayısı")
        plt.show()
